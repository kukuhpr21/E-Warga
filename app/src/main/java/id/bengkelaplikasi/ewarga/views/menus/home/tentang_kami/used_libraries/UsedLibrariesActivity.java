package id.bengkelaplikasi.ewarga.views.menus.home.tentang_kami.used_libraries;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.bengkelaplikasi.ewarga.R;
import id.bengkelaplikasi.ewarga.main.App;
import id.bengkelaplikasi.ewarga.views.menus.home.tentang_kami.used_libraries.adapter.DetailLibrariesAdapter;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Kukuh182 on 25-Sep-17
 * Bengkel Aplikasi
 * kukuhpr21@gmail.com
 */

public class UsedLibrariesActivity extends AppCompatActivity implements UsedLibrariesView {

    private UsedLibrariesPresenter presenter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private View view;

    @BindView(R.id.toolbar_library)Toolbar toolbar_library;
    @BindView(R.id.rv_libraries)RecyclerView rv_libraries;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usedlibraries);
        onAttachView();
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    @Override
    public void onAttachView() {
        presenter = new UsedLibrariesPresenter();
        presenter.onAttach(this);
        ButterKnife.bind(this);
        initData();
        addActionNavigationClose();
    }

    @Override
    public void onDetachView() {
        presenter.onDetach();
    }

    @Override
    protected void onDestroy() {
        onDetachView();
        super.onDestroy();
    }

    @Override
    public void initData() {
        rv_libraries.getRecycledViewPool().clear();
        rv_libraries.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(App.getContext());
        rv_libraries.setLayoutManager(layoutManager);
        adapter = new DetailLibrariesAdapter(presenter.getlistLibraries());
        rv_libraries.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void addActionNavigationClose() {
        toolbar_library.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDettaillibrary();
            }
        });
    }

    @Override
    public void closeDettaillibrary() {
        finish();
        overridePendingTransition(R.transition.pull_in_left, R.transition.push_out_right);
    }
}
