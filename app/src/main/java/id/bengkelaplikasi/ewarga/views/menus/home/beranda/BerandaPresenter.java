package id.bengkelaplikasi.ewarga.views.menus.home.beranda;

import java.util.ArrayList;

import id.bengkelaplikasi.ewarga.R;
import id.bengkelaplikasi.ewarga.views.base.Presenter;
import id.bengkelaplikasi.ewarga.views.menus.home.beranda.adapter.MenuBerandaMA;

/**
 * Created by Kukuh182 on 25-Sep-17
 * Bengkel Aplikasi
 * kukuhpr21@gmail.com
 */

public class BerandaPresenter implements Presenter<BerandaView>{

    private BerandaView mView;

    @Override
    public void onAttach(BerandaView view) {
        this.mView = view;
    }

    @Override
    public void onDetach() {
        this.mView = null;
    }

    public ArrayList<MenuBerandaMA> initListMenuBeranda(){
        int [] icons = {R.drawable.acara, R.drawable.peta, R.drawable.pengaduan, R.drawable.pelayanan, R.drawable.daftar_warga, R.drawable.penanda_lokasi};
        String [] menus = {"ACARA",  "PETA", "PENGADUAN", "PELAYANAN", "DAFTAR WARGA", "TANDAI LOKASI"};
        ArrayList<MenuBerandaMA> data = new ArrayList<>();
        for (int i = 0;i<menus.length; i++){
            MenuBerandaMA model = new MenuBerandaMA();
            model.setIcon(icons[i]);
            model.setDescription(menus[i]);
            data.add(model);
        }
        return data;
    }

    public void checkMenu(String menu){
        switch (menu){
            case "ACARA":
                mView.callAcara();
                break;
            case "PETA":
                mView.callPeta();
                break;
            case "PENGADUAN":
                mView.callPengaduan();
                break;
            case "PELAYANAN":
                break;
            case "DAFTAR WARGA":
                mView.callDaftarWarga();
                break;
            case "TANDAI LOKASI":
                break;

        }
    }
}
